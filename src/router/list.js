
export const routes = [
  {
    path: '/',
    redirect: 'dashboard',
    meta: {
    },
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import('@/views/user/Dashboard.vue'),
    meta: {
    },
  },
  {
    path: '/error-404',
    name: 'error-404',
    component: () => import('@/views/Error.vue'),
    meta: {
    },
  },
  {
    path: '*',
    redirect: 'error-404',
    meta: {
    },
  },
]