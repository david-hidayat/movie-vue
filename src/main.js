import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import axios from "axios";
import router from "./router";
import store from "./store";
import HorizontalScroll from 'vue-horizontal-scroll'
import 'vue-horizontal-scroll/dist/vue-horizontal-scroll.css'
import RadialProgressBar from 'vue-radial-progress'
import moment from "moment";

Vue.component('horizontal-scroll', HorizontalScroll)
Vue.component('radial-progress-bar', RadialProgressBar)
Vue.config.productionTip = false;
Vue.prototype.$moment = moment;
Vue.prototype.$http = axios;
Vue.prototype.$http.defaults.headers.common['Authorization'] = `Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3NjAwNGViNDAzZmEwZDhjZjExMzI3MmFmMWJjZTQxYiIsInN1YiI6IjVkMjEyYmQxMTVjNjM2MTcyOWY2YWUyNyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.9_fPGmTUcZMD58kz5w2RthO0QpOK4_jd6hQGEUUlQe4`;

Vue.directive("click-outside", {
    bind(el, binding, vnode) {
        console.log(el, binding, vnode);
        el.clickOutsideEvent = (event) => {
            if (!(el === event.target || el.contains(event.target))) {
                vnode.context[binding.expression](event);
            }
        };
        document.addEventListener("click", el.clickOutsideEvent);
        document.addEventListener("touchstart", el.clickOutsideEvent);
    },
    unbind(el) {
        document.removeEventListener("click", el.clickOutsideEvent);
        document.removeEventListener("touchstart", el.clickOutsideEvent);
    },
});
new Vue({
    router,
    store,
    vuetify,
    render: (h) => h(App)
}).$mount("#app");